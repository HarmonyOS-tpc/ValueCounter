# ValueCounter

## 自定义组件功能

   实现组件计数器功能

## 导入依赖

方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码

        implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：

          allprojects{
               repositories{
                   mavenCentral()
               }
          }
          implementation 'io.openharmony.tpc.thirdlib:valueCounter:1.0.4'

## Step2

        <com.mohammedalaa.valuecounterlib.ValueCounterView
                ohos:id="$+id:valueCounter"
                ohos:cornerRadius="15vp"
                ohos:defaultValue="5"
                ohos:maxValue="10"
                ohos:minValue="1"
                ohos:outlineColor="$color:colorAccent"
                ohos:stepValue="1"
                ohos:strokeWidth="2vp"
                ohos:valueColor="$color:back"
                ohos:valueTextSize="12fp"
                ohos:width="match_content"
                ohos:height="match_content"
                ohos:center_in_parent="true"
                />

## Step3
   Reference the View in Java code.

        valueCounterView = (ValueCounterView) findComponentById(ResourceTable.Id_valueCounter);

   getValue

        valueCounterView.getValue();


## 示例截图
![image](https://gitee.com/openharmony-tpc/ValueCounter/raw/master/screenshot/test.gif)



## entry运行要求
通过DevEco studio,并下载SDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）
